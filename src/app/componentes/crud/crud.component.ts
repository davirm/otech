import { Cep } from './../../model/placeholder.model';
import { CrudService } from './../../services/crud.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {
  cep: Cep;
  erro: any;
  constructor(private crudService: CrudService) { 
   // this.getter();
  }

  ngOnInit() {
  }

  // getter(){
  //   this.crudService.getCep().subscribe(
  //     (data: Cep) => {
  //       this.cep = data;
  //       console.log("data", data);
  //       console.log("cep", this.cep);
  //     }, error => {
  //       this.erro = error;
  //       console.log("erro", error)
  //     }
  //   );
  // }
}
