import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    setTimeout(()=>{
        window.location.href = window.location.origin;
    }, 2000);
  }


}
