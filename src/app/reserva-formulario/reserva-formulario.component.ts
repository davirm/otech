import { Filmes, Cep } from './../model/placeholder.model';
import { CrudService } from './../services/crud.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-reserva-formulario',
  templateUrl: './reserva-formulario.component.html',
  styleUrls: ['./reserva-formulario.component.css']
})
export class ReservaFormularioComponent implements OnInit {
    registerForm: FormGroup;
    submitted = false;
    filmes: Filmes;
    erro: any;
    filme: {};
    frete: 10.00;
    dadosPessoais: {};
    cep:"000";
    telefone: "0000";
    alertMensage: boolean = false ; 
    constructor(private crudService: CrudService) { 
        this.getter();
    }

  ngOnInit() { 
        this.frete = 10.00;
        this.dadosPessoais = new FormGroup({
            primeiroNome: new FormControl('', Validators.required),
            segundooNome: new FormControl('', Validators.required)
        });
    }
  
  ChangeFilme(val: any ){
    this.filme = JSON.parse(val);
  }

    changeInput(event: any){
      let value = event.target.value;
      let name = event.target.id;
      let mascara = event.target.dataset;
      var formgroup = document.querySelectorAll(".box-form [required]");
      let thisElement = document.querySelector("#" + name);
        
        if(mascara.mask == 'cpf'){
            this[name] = value.replace(/\D/g,"").replace(/(\d{3})(\d)/,"$1.$2").replace(/(\d{3})(\d)/,"$1.$2").replace(/(\d{3})(\d{1,2})$/,"$1-$2");
            if(value.length == 14){
                if( !this.testaCPF(value.replace(/\D/g,"").replace('-', '')) ){
                    this[name] = '';
                }
            }
        } else if(mascara.mask == 'data'){
			this[name] = value.replace(/\D/g,"").replace(/(\d{2})(\d)/,"$1/$2").replace(/(\d{2})(\d)/,"$1/$2");
        } else if(mascara.mask == 'cep'){
            this[name] = value.replace(/\D/g,"").replace(/(\d{5})(\d)/,"$1-$2");
            if(value.length == 9){
                this.crudService.getCep(value).subscribe(
                    (data: Cep) => {
                        if(data['cep']){
                            this['pais'] = 'Brasil';
                            this['estado'] = 'DF';
                            this['cidade'] = data["bairro"] + ' - ' + data['localidade'];
                            this['endereco'] = data['logradouro'];
                            
                            document.querySelector("#pais").classList.remove("has-error");
                            document.querySelector("#estado").classList.remove("has-error");
                            document.querySelector("#cidade").classList.remove("has-error");
                            document.querySelector("#endereco").classList.remove("has-error");
                        } else{
                            this[name] = '';
                        }
                    }, error => {
                        this.erro = error;
                    }
                );
            }
        } else if(mascara.mask == 'telefone'){
            if(value.length <= 4){
                this[name] = value.replace(/\D/g,"").replace(/^(\d)/,"($1").replace(/(.{3})(\d)/,"$1)$2");
            } else if(value.length > 8 && value.length <= 13){
                this[name] = value.replace('-', '').replace(/(\d{4})(\d)/,"$1-$2");
            } else if(value.length > 13){
                this[name] = value.replace('-', '').replace(/(\d{4})(\d)/,"$1-$2");
            }
        } else{
            this[name] = value;
        }
        if(value){
          thisElement.classList.remove("has-error");
        }
    }
    
    enviarFormulario(){
        var formgroup = document.querySelectorAll(".box-form [required]");
        formgroup.forEach(function(formItem) {
			if(!formItem['value']){
				formItem.classList.add("has-error")	
            }
        });
		var formErro = document.querySelectorAll(".box-form .has-error");
        if(!formErro.length){
            var formgroup = document.querySelectorAll(".box-form [required]");
            var infoFormulario = new Array();
            var valorTotal = (this['acompanhante'])?(this['filme']['valor']*2)+this["frete"]:this['filme']['valor']+this["frete"];
            infoFormulario.push({titulo: this['filme']['titulo'], valor: valorTotal})
            formgroup.forEach(function(formItem, index) {
                infoFormulario.push({[formItem.id]: formItem['value']})
                if((index + 2 ) == formgroup.length){
                   console.log(infoFormulario, formItem.id, window.location)
                   window.location.href = window.location.origin + '/reserva';
                }
            });
        } else{
            this.alertMensage = true;
            setTimeout(()=>{
                this.alertMensage = false;
           }, 2000);
        }
    }
    
    testaCPF(strCPF) {
        var Soma;
        var Resto;
        Soma = 0;
        if (strCPF == "00000000000"){ return false;}
         
        for (var i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto == 10) || (Resto == 11)){  Resto = 0};
        if (Resto != parseInt(strCPF.substring(9, 10)) ){ return false};
    
        Soma = 0;
        for (var i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
    
        if ((Resto == 10) || (Resto == 11))  Resto = 0;
        if (Resto != parseInt(strCPF.substring(10, 11) ) ){ return false;}else{return true;}
    }
    
    getter(){
        this.crudService.getFilmes().subscribe(
            (data: Filmes) => {
            this.filmes = data;
            this.filme = data[0]
        }, error => {
            this.erro = error;
        }
      );
    }
}