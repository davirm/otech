import { ReservaComponent } from './reserva/reserva.component';
import { FilmesComponent } from './filmes/filmes.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReservaFormularioComponent } from './reserva-formulario/reserva-formulario.component';


const routes: Routes = [
  {
    path: "formulario",
    component: ReservaFormularioComponent
  },
  {
    path: "",
    component: FilmesComponent
  },
  {
    path: "reserva",
    component: ReservaComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
