import { Filmes } from './../model/placeholder.model';
import { CrudService } from './../services/crud.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-filmes',
  templateUrl: './filmes.component.html',
  styleUrls: ['./filmes.component.css']
})
export class FilmesComponent implements OnInit {
	filmes: Filmes;
	erro: any;
	
 	constructor(private crudService: CrudService) {
    	this.getter();
   	}

  	ngOnInit() {
  	}
	
	reservar(info){
		window.location.href = window.location.origin + '/formulario';
	}

	getter(){
		this.crudService.getFilmes().subscribe(
			(data: Filmes) => {
			  	this.filmes = data;
		  	}, error => {
			  	this.erro = error;
		  	}
		);
	}

}