import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Images } from './../model/placeholder.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class CrudService {
  constructor(private http: HttpClient) { }
  public getFotos(): Observable<any> {
    return this.http.get('https://jsonplaceholder.typicode.com/photos');
  }
  public getFilmes(): Observable<any> {
    return this.http.get('./../assets/filmes.json');
  }
  public getCep(paramet): Observable<any> { 
    return this.http.get('https://viacep.com.br/ws/'+ paramet +'/json/');
  }
  
}

//return this.http.get('http://correiosapi.apphb.com/cep/72235222');
// return this.http.get('https://jsonplaceholder.typicode.com/photos');