// export class Images {
//     public albumId: number;
//     public id: number;
//     public title: string;
//     public url: string;
//     public thumbnailUrl: string;
// }

export class Images {
    public bairro: string;
    public cep: number;
    public cidade: string;
    public estado: string; 
    public logradouro: string;
    public tipodelogradouro: string;
}

export class Filmes {
    nome: string = '';
    tipo: number = 1;
    genero: number = 1;
    dataNascimento: Date = null;
    observacao: string = '';
    inativo: boolean = false;
  }
  
export class Cep {
    nome: string = '';
    tipo: number = 1;
    genero: number = 1;
    dataNascimento: Date = null;
    observacao: string = '';
    inativo: boolean = false;
  }